// =============== DO NOT DELETE ================
// Default task providing project initialization.

"use strict";

const Process = require("process");
const Path = require("path");
const Fs = require("fs");
const PackageManager = require("uu_appg01_devkit-common/src/tools/package-manager.js");

module.exports = async function (environment, ...args) {
  // Load project descriptor
  let projectRoot = Process.env["INIT_CWD"] || Process.cwd();
  let projectPackageJsonPath = Path.join(projectRoot, "package.json");
  if (!Fs.existsSync(projectPackageJsonPath)) {
    console.error("ERROR: Plugin initialization must be called in project root.\n");
    return;
  }
  let projectPackageJson = JSON.parse(Fs.readFileSync(projectPackageJsonPath));

  // Load plugin descriptor
  let selfPackageJsonPath = Path.join(__dirname, "..", "..", "package.json");
  let selfPackageJson = JSON.parse(Fs.readFileSync(selfPackageJsonPath));
  if (selfPackageJson.name === projectPackageJson.name) {
    return; // do not install into itself
  }
  console.log(`Adding ${selfPackageJson.name} plugin to ${projectPackageJson.name}:`);

  // Add plugin as development dependency (if not already present)
  console.log("  Updating project dependencies...");
  let devDependencies = projectPackageJson.devDependencies;
  if (!devDependencies) {
    devDependencies = {};
    projectPackageJson.devDependencies = devDependencies;
  }
  if (!devDependencies[selfPackageJson.name]) {
    devDependencies[selfPackageJson.name] = `^${selfPackageJson.version}`;
  }

  // Load scripts declared in project
  console.log("  Adding npm tasks provided by plugin...");
  let scripts = projectPackageJson.scripts;
  if (!scripts) {
    scripts = {};
    projectPackageJson.scripts = scripts;
  }

  // Compute task prefix (from plugin project name)
  let taskPrefix = selfPackageJson.name
    .replace(/-devkit$/, "")
    .replace(/[\-_][\w0-9]/g, (match) => {
      return `${match[1].toUpperCase()}`;
    })
    .replace(/g[0-9]{2}/, "");

  // List all task sripts from plugin
  let tasks = Fs.readdirSync(__dirname);
  tasks.forEach((taskSrc) => {
    // Ignore tasks starting with underscore
    if (!taskSrc.startsWith("_")) {
      // Build task name
      let task = taskSrc.split(".")[0].replace(/(-[\w0-9])/g, (match) => {
        return `${match[1].toUpperCase()}`;
      });
      // Build task qualified name (with plugin project prefix)
      let qualifiedName = `${taskPrefix}${task[0].toUpperCase()}${task.slice(1)}`;
      let command = `${selfPackageJson.name} ${task}`;
      if (!scripts[qualifiedName]) {
        // Add task if not already present
        scripts[qualifiedName] = command;
      } else if (scripts[qualifiedName] !== command) {
        console.warn(`  WARNING: Project already contains custom task \"${qualifiedName}\", skipping.`);
      }
    }
  });

  // Store updated project descriptor
  Fs.writeFileSync(projectPackageJsonPath, JSON.stringify(projectPackageJson, null, 2));

  // Install plugin - install only in case installation is not disabled and project has already installed dependencies
  let nodeModulesPath = Path.join(projectRoot, "node_modules");
  if (Fs.existsSync(nodeModulesPath) && !args.includes("skipinstall")) {
    console.log("  Installing plugin and its dependencies...");
    await PackageManager.install();
  }

  console.log(`Project successfully initialized.\n`);
};

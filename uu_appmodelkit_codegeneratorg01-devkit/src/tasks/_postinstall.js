// =============== DO NOT DELETE ================
// Default task providing project initialization.

"use strict";

const Path = require("path");

let initTaskSrc = Path.join(__dirname, "_init.js");
require(initTaskSrc)("development", "skipinstall");

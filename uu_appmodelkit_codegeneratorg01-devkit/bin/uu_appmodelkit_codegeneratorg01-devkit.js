#! /usr/bin/env node
"use strict";

const Process = require("process");
const Path = require("path");
const Fs = require("fs");

let args = Process.argv.slice(2);

// Task should be given as first argument
let task;
let taskSrc;
if (args[0]) {
  task = args[0].replace(/([a-z0-9][A-Z])/g, match => {
    return `${match[0]}-${match[1].toLowerCase()}`;
  });
  taskSrc = Path.join(__dirname, "..", "src", "tasks", `${task}.js`);
}
// If first argument is valid task...
if (task && Fs.existsSync(taskSrc)) {
  // ... remove task from arguments
  args = args.splice(1);
} else {
  // ... else run default task
  task = "_init";
  taskSrc = Path.join(__dirname, "..", "src", "tasks", `${task}.js`);
}

// Parse environment from arguments
let environment;
let argsLine = args.join(" ");
let envMatch = argsLine.match(/--env(?:ironment)?[\s=]([^\s]*)/);
if (envMatch) {
  // If environment is declared, remove it from arguments
  environment = envMatch[1];
  args = argsLine.replace(/--env(?:ironment)?[\s=]([^\s]*)/, "").split(/\s+/);
} else {
  // Else use default environment
  environment = "development";
}

// Invoke task, pass environment and remaining arguments
require(taskSrc)(environment, ...args);
